<?php
namespace App\Repositories\ChallengeOne;

class BestScoreRepository {
    public function getBestScore($file) {
        $content = file_get_contents($file);
        $values = explode(PHP_EOL, $content);
        // Declare and init vars incase data was not send
        $rounds =  $values[0];
        unset($values[0]);
        $scores1 = [];
        $scores2 = [];

        foreach($values as $value) {
            $data = explode(' ', $value);
            array_push($scores1, $data[0]);
            array_push($scores2, $data[1]);
        }
        ////

        // Process comparing scores
        $currentData = []; // Collect each difference per round
        foreach($scores1 as $i => $score1) {
            $score2 = $scores2[$i]; // Get player 2 score
            if($score1 > $score2) { // If player 1 has more score
                $currentData[$score1-$score2] = 1; // Current data fill with thw difference as Key and Number player ass value
            } else { // If player 2 has more score
                $currentData[$score2-$score1] = 2; // Current data fill with thw difference as Key and Number player ass value
            }
        }

        $maxScore = max(array_keys($currentData)); // Get the max score comparing the keys that contain each difference
        $winner = $currentData[$maxScore]; // Get the player winner by the greatest key getted in last step

        // Creating file of response
        $path = __DIR__.'/../../../public/problem2.txt'; // Path and name to save
        $fp = fopen($path, "wb"); // Open for write content
        fwrite($fp, "$winner $maxScore"); // Write content
        fclose($fp); // Close file
        $type = 'text/txt'; // Add MIME Type
        $headers = ['Content-Type' => $type]; // Add headers
        ////
    
        return response()->download($path, 'problem2.txt', $headers);; // Return the results in response
    }
}