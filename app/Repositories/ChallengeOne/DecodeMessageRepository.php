<?php
namespace App\Repositories\ChallengeOne;

class DecodeMessageRepository {
    
    public function decode($file) {
        $content = file_get_contents($file);
        $values = explode(PHP_EOL, $content);
        // Declare and init vars incase data was not send
        $charCount = explode(' ' ,$values[0]);
        $firstSentence = $values[1];
        $secondSentence = $values[2];
        $hash = $values[3];
        $content = ''; // Declare response var as array
        ////
    
        // Process of decode message
        $hashArray = str_split(strtolower($hash)); // Turning hash string into array
        $lastElement = ''; // Declare lastchar variable for checking dupe case
        foreach($hashArray as $key => $eachHash) {
            if($eachHash == $lastElement) unset($hashArray[$key]); // Remove element from array incase dupe
            $lastElement = $eachHash; // Save the last char for comparison
        }
        $newHash = implode($hashArray); // Turnig new hash array into string
    
        $levelComparison1 = similar_text($firstSentence, $newHash); // Checking similarity percentage from first message
        $levelComparison2 = similar_text($secondSentence, $newHash); // Checking similarity percentage from second message

        if($levelComparison1 > $levelComparison2) { // Decode process incase first message has more similarity percentage
            $index = strpos($newHash, strtolower(substr($firstSentence, 0, 2))); // Getting index of first char in message on new hash
            $wordInHash = substr($newHash, $index, $charCount[0]); // Getting the message by last index getted and count of chars from message
            if($wordInHash == strtolower($firstSentence)) { // If result was the same set the response to first message YES and second NO
                $content = "SI\nNO";
            } else { // Incase does not match, set the response as noone message is in hash
                $content = "NO\nNO";
            }
        } else { // Decode process incase second message has more similarity percentage
            $index = strpos($newHash, strtolower(substr($secondSentence, 0, 21))); // Getting index of first char in message on new hash
            $wordInHash = substr($newHash, $index, $charCount[1]); // Getting the message by last index getted and count of chars from message
            if($wordInHash == strtolower($secondSentence)) { // If result was the same set the response to second message YES and first NO
                $content = "NO\nSI";
            } else { // Incase does not match, set the response as noone message is in hash
                $content = "NO\nNO";
            }
        }
        ////
        // Creating file of response
        $path = __DIR__.'/../../../public/problem1.txt'; // Path and name to save
        $fp = fopen($path, "wb"); // Open for write content
        fwrite($fp, $content); // Write content
        fclose($fp); // Close file
        $type = 'text/txt'; // Add MIME Type
        $headers = ['Content-Type' => $type]; // Add headers
        ////
    
        return response()->download($path, 'problem1.txt', $headers);; // Return the results in response
    }
}
?>