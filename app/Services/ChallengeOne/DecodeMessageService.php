<?php

namespace App\Services\ChallengeOne;
use Illuminate\Support\Facades\Log;
use App\Repositories\ChallengeOne\DecodeMessageRepository;


class DecodeMessageService {

    protected $decodeMessageRepository;

    public function __construct(DecodeMessageRepository $decodeMessageRepository) {
        $this->decodeMessageRepository = $decodeMessageRepository;
    }

    public function decode($request) {
        try {
            $file = $request['file'];
            return $this->decodeMessageRepository->decode($file);
        } catch (\Throwable $th) {
            throw $th;
            Log::alert($th);
            return response()->json(['error' => true, 'descripcion' => 'Error Interno']);
        }
    }
}