<?php

namespace App\Services\ChallengeOne;
use Illuminate\Support\Facades\Log;
use App\Repositories\ChallengeOne\BestScoreRepository;


class BestScoreService {

    protected $bestScoreRepository;

    public function __construct(BestScoreRepository $bestScoreRepository) {
        $this->bestScoreRepository = $bestScoreRepository;
    }

    public function getBestScore($request) {
        try {
            $file = $request['file'];
            return $this->bestScoreRepository->getBestScore($file);
        } catch (\Throwable $th) {
            throw $th;
            Log::alert($th);
            return response()->json(['error' => true, 'descripcion' => 'Error Interno']);
        }
    }
}