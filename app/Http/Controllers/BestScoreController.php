<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Services\ChallengeOne\BestScoreService;

class bestScoreController extends Controller {
    protected $bestScoreService;

    public function __construct(BestScoreService $bestScoreService) {
        $this->bestScoreService = $bestScoreService;
    }
    
    public function getBestScore(Request $request) {
        try {
            if($request['file']) {
            return $this->bestScoreService->getBestScore($request);
            } else {
                return response()->json(['error' => true, 'descripcion' => 'Falta archivo'], 401);    
            }
        } catch (\Throwable $th) {
            throw $th;
            Log::alert($th);
            return response()->json(['error' => true, 'descripcion' => 'Error Interno']);
        }
    }
}