<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Services\ChallengeOne\DecodeMessageService;

class DecodeMessageController extends Controller {

    protected $decodeMessageService;

    public function __construct(DecodeMessageService $decodeMessageService) {
        $this->decodeMessageService = $decodeMessageService;
    }

    public function decode(Request $request) {
        try {
            if($request['file']) {
                return $this->decodeMessageService->decode($request);
            } else {
                return response()->json(['error' => true, 'descripcion' => 'Falta archivo'], 401);    
            }
        } catch (\Throwable $th) {
            throw $th;
            Log::alert($th);
            return response()->json(['error' => true, 'descripcion' => 'Error Interno'], 500);
        }
    }
}
?>