# Challenge 1

Aplicación web desarrollada en Lumen de PHP para resolver los ejercicios del Challenge 1 de Neubox.

## Requerimientos

```
php > 7.3
Lumen 8.1
```

## Instalación

Utilizar el administrador de paquetes por cmd de [Composer](https://getcomposer.org/download/) para descargar las librerías necesarias de [Lumen 8.1](https://lumen.laravel.com/docs/8.x/installation).

```
1. Descargar el repositorio
2. Ingresar a la carpeta
3. Ejecutar el comando composer install 
```

## Ejecución
Una vez descargadas las librerías necesarias, ejecutaremos un servidor local para ingresar a nuestra aplicación.

```
php -S localhost:8000 -t public
```

## Uso
Se generaron dos rutas (una para cada problema), para acceder a ellas se requiere lo siguiente:

## Problema 1: Mensajes codificados

|      Ruta      | Método|  Parámetro |    Tipo    |      Ejemplo      |
| -------------- | ------| ---------- | ---------- | ----------------- |
| /decodeMessage | POST  | charsCount | Array[int] |    [5, 12, 45]    |
|                |       | first      | String     |    "FraseUno"     |
|                |       | second     | String     |    "PhraseTwo     |
|                |       | hash       | String     | "XxxFfrraseUUnoo" |

## Problema 2: Ganador por mayor ventaja

|      Ruta      | Método|  Parámetro |    Tipo    |      Ejemplo      |
| -------------- | ------| ---------- | ---------- | ----------------- |
|  /bestScore    | POST  |   rounds   |  Integer   |         3         |
|                |       |  scores1   | Array[int] |    [12, 13, 23]   |
|                |       |  scores2   | Array[int] |    [6, 23, 13]    |
